import './App.css';

function App() {
  return (
    <div className="App" data-testid="app-component">
      Test Content
    </div>
  );
}

export default App;
