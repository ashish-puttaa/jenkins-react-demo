import { render, screen } from '@testing-library/react';
import App from './App';

test('renders content', () => {
  render(<App />);
  const targetElement = screen.getByTestId("app-component");
  expect(targetElement.textContent).toBe("Test Content");
});
